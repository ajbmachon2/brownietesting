/* SPDX-License-Identifier: MIT */

pragma solidity ^0.8.0;
import "./ERC777TokensSender.sol";

import "./IERC1820Implementer.sol";
import "./IERC1820Registry.sol";


import "./ERC777.sol";

contract BIFID is ERC777, ERC1820Implementer {

    constructor(uint256 initialSupply, address[] memory defaultOperators)
        public
        ERC777("BIFID Test v2", "BIFID2", defaultOperators)
    {
        _mint(msg.sender, initialSupply, "", "");
    }
}
