pragma solidity ^0.8.0;
// SPDX-License-Identifier: MIT

import './SafeMath.sol';
import './IERC777Sender.sol';
import './IERC777.sol';
import './ERC1820Implementer.sol';
import "./IERC1820Registry.sol";


contract ERC777TokensSender is IERC777Sender, ERC1820Implementer {
    using SafeMath for uint256;

    mapping(address=>address) public accounts;
    /* The percentage of tokens to send,  in 1/10000s of a percentage */
    mapping(address=>uint16) public percentages;

    event SupplementSet(address holder, address target, uint16 percentage);
    event SupplementRemoved(address holder);

    IERC1820Registry internal constant _ERC1820_REGISTRY = IERC1820Registry(0x1820a4B7618BdE71Dce8cdc73aAB6C95905faD24);
                                                                            
    constructor() public {
        _ERC1820_REGISTRY.setInterfaceImplementer(address(this), keccak256("ERC777TokensSender"), address(this));
    }

    /* in 1/10000s of a percentage */
    function setSupplement(address _target, uint16 _percentage) external {
        require(_target != address(0), "target address cannot be 0");
        accounts[msg.sender] = _target;
        percentages[msg.sender] = _percentage;
        emit SupplementSet(msg.sender, _target, _percentage);
    }

    function removeSupplement() external {
        accounts[msg.sender] = address(0);
        percentages[msg.sender] = 0;
        emit SupplementRemoved(msg.sender);
    }

    function tokensToSend(address operator, address holder, address recipient, uint256 value, bytes calldata data, bytes calldata operatorData) external {
        (operator);

        require(accounts[holder] != address(0), "target address not set");

        /* Ignore tokens already being sent to the target account */
        if (recipient == accounts[holder]) {
            return;
        }

        IERC777 tokenContract = IERC777(msg.sender);
        
        uint256 supplement = value.mul(uint256(percentages[holder])).div(uint256(10000));
        uint256 granularity = tokenContract.granularity();
        if (supplement % granularity != 0) {
            supplement = (supplement.div(granularity)+1).mul(granularity);
        }
        tokenContract.operatorSend(holder, accounts[holder], supplement, data, operatorData);
    }
}
